let webpack = require('webpack')
let name = 'homepage'
name = 'password'

module.exports = {
    entry: `./src/js/${name}/index.js`,
    output: {
        path:  `${__dirname}/src/js/${name}/`,
        filename: 'app.js'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    },

    // plugins:[
   	//  new webpack.DefinePlugin({
   	//    'process.env':{
   	//      NODE_ENV: JSON.stringify('production')
   	//    }
   	//  }),
   	//  new webpack.optimize.UglifyJsPlugin()
   	// ]
}
