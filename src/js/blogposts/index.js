import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            cards: null
        }
    }

    componentDidMount() {
        this.setState({
            cards: ['item0', 'item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8', 'item9']
        })
    }

    render() {
        let _cards = this.state.cards

        return (
            <div className="blogposts">
                {this.getHeader('文章列表')}
                <Content items={_cards}/>
            </div>
        )
    }
}

class Content extends Component {

    goArticleView(index) {
        let url = "articleview.html"
        let ws = plus.webview.create(url, url, null, {index: index}) //id 也使用 url
        plus.webview.show(ws, "slide-in-right")
    }

    getItems() {
        if (!this.props.items) return null

        let _goArticleView = this.goArticleView.bind(this)
        let _items = this.props.items.map((item, index) => {
            return (
                <div className="card" key={index} onClick={() => _goArticleView(index)}>
                    <img className="img" src="img/scenery.jpg" />
                    <p>
                        这里填写说明文字。。。BalaBala...<br />
                    </p>
                    <div className="container-origin">
                        <div className="container-title">
                            <div className="title">Zwei</div>
                            <div className="time">5 minutes ago</div>
                        </div>
                        <img className="origin-header" src="img/octocat.png" />
                    </div>
                </div>
            )
        })

        return _items
    }

    render() {
        let _items = this.getItems()

        return (
            <div className="content-container grey lighten-5">
                {_items}
            </div>
        )
    }
}

ReactDom.render(<App />, $('#app')[0])