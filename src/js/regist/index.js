import React, {Component} from 'react'
import Root from '../util/root'
import ReactDom from 'react-dom'

class App extends Root {

    regist() {
        let ws = plus.webview.create('loginv1.html', 'loginv1.html', null, {})
        plus.webview.show(ws || 'loginv1.html', "slide-in-right")
    }

    render() {

        return (
            <div className="regist">
                <img className="header" src="../src/img/octocat.png" />
                <div className="row">
                    <div className="input-field col s12">
                        <input id="username" type="text" className="validate" />
                            <label htmlFor="username">Username</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="email" type="email" className="validate" />
                            <label htmlFor="email">Email</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="password" type="password" className="validate" />
                        <label htmlFor="password">Password</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="password" type="password" className="validate" />
                            <label htmlFor="password">Confirm Password</label>
                    </div>
                </div>
                <a className="waves-effect waves-light btn">login</a>
                <div className="sign">
                    已有账号？
                    <a className="regist" onClick={this.regist}>
                        点击登录
                    </a>
                </div>
            </div>
        )
    }
}

class M {
}

ReactDom.render(<App />, $('#app')[0])
