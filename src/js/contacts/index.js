import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            contacts: null
        }
        this.initialContacts = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine']
    }

    componentDidMount() {
        this.setState({
            contacts: this.initialContacts
        })
    }

    searchChange(e) {
        let _value = e.target.value.toLowerCase()
        let _newContacts = []
        for(let i = 0, j = this.initialContacts.length; i < j; i++) {
            let _contacts = this.initialContacts[i]
            if(_contacts.toLowerCase().includes(_value)) _newContacts.push(_contacts)
        }

        this.setState({
            contacts: _newContacts
        })
    }

    render() {
        let _contacts = this.state.contacts

        return (
            <div className="contacts">
                {this.getHeader('通知消息')}
                <Content items={_contacts}
                         searchChange={this.searchChange.bind(this)} />
            </div>
        )
    }
}

class Content extends Component {

    getItems() {
        if (!this.props.items) return null
        let _items = this.props.items.map((item, index) => {
            return (
                <div className="label" key={index}>
                    <img className="image" src="img/octocat.png"/>
                    <span className="name">{item}</span>
                </div>
            )
        })

        return _items
    }

    render() {
        let _items = this.getItems()
        let _searchChange = this.props.searchChange

        return (
            <div className="content-container">
                <div className="input-field">
                    <i className="material-icons prefix">account_circle</i>
                    <input id="search" type="text" className="validate"
                    onChange={_searchChange} />
                    <label htmlFor="search">Search</label>
                </div>
                {_items}
            </div>
        )
    }
}

ReactDom.render(
    <App />
    , $('#app')[0])