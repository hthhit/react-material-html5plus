import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            itemNames: null
        }
    }

    componentDidMount() {
        document.addEventListener('plusready', () => {
            this.setState({
                itemNames: plus.webview.currentWebview().list
            })
        }, false)
    }

    render() {
        let _itemNames = this.state.itemNames

        return (
            <div className="userprofilev1">
                {this.getHeader('用户页 V1')}
                <Content items={_itemNames}/>
                <span className="other">其他内容...</span>
            </div>
        )
    }
}

class Content extends Component {
    nextPage(url) {
        let ws = plus.webview.create(url, url, null, {})
        plus.webview.show(ws, "slide-in-right")
    }

    items() {
        if (!this.props.items) return null

        let _items = this.props.items.map((item, index) => {
            let url = item.url

            return (
                <a key={index} href="#!" className="collection-item" onClick={() =>this.nextPage(url)}>{item.name}</a>
            )
        })
        return _items
    }

    render() {
        return (
            <div className="card sticky-action">
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src="img/octocat.png" />
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">UI Koala<i className="material-icons right">more_vert</i></span>

                    <p><a href="#!">关注我</a></p>
                </div>

                <div className="card-action">
                    <a href="#">跟我聊天</a>
                    <a href="#">发送信息</a>
                </div>

                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">UI Koala<i className="material-icons right">关闭</i></span>
                    <p>基于 React, Materialize, HTML5Plus 的 UI 框架</p>
                </div>
            </div>
        )
    }
}

class M {
}

ReactDom.render(<App />, $('#app')[0])