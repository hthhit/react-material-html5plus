import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            itemNames: null
        }
    }

    componentDidMount() {
        document.addEventListener('plusready', () => {
            this.setState({
                itemNames: plus.webview.currentWebview().list
            })
        }, false)
    }

    render() {
        let _itemNames = this.state.itemNames

        return (
            <div className="list">
                {this.getHeader('模板列表')}
                <Content items={_itemNames}/>
            </div>
        )
    }
}

class Content extends Component {
    nextPage(url) {
        let ws = plus.webview.create(url, url, null, {})
        plus.webview.show(ws, "slide-in-right")
    }

    items() {
        if (!this.props.items) return null

        let _items = this.props.items.map((item, index) => {
            let url = item.url

            return (
                <a key={index} href="#!" className="collection-item" onClick={() =>this.nextPage(url)}>{item.name}</a>
            )
        })
        return _items
    }

    render() {
        return (
            <div className="collection">
                {this.items()}
            </div>
        )
    }
}

class M {
}

ReactDom.render(<App />, $('#app')[0])