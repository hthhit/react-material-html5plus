import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            cards: null
        }
    }

    componentDidMount() {
        this.setState({
            cards: ['item0', 'item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8', 'item9']
        })
    }

    render() {
        let _cards = this.state.cards

        return (
            <div className="cards">
                {this.getHeader('卡片内容')}
                <Content items={_cards}/>
            </div>
        )
    }
}

class Content extends Component {

    getItems() {
        if (!this.props.items) return null
        let _items = this.props.items.map((item, index) => {
            return (
                <div className="card large" key={index}>
                    <div className="card-image">
                        <img className="image" src="img/octocat.png" />
                        <span className="card-title">{item}</span>
                    </div>
                    <div className="card-content">
                        <p>我是一个非常简单的卡片。 我善于包含小部分内容。 我很方便，因为我需要的标记很少。</p>
                    </div>
                    <div className="card-action">
                        <a href="#"><i className="material-icons">star</i></a>
                        <a href="#"><i className="material-icons">chat_bubble_outline</i></a>
                        <a href="#"><i className="material-icons">perm_identity</i></a>
                    </div>
                </div>
            )
        })

        return _items
    }

    render() {
        let _items = this.getItems()

        return (
            <div className="content-container grey lighten-5">
                {_items}
            </div>
        )
    }
}

ReactDom.render(
    <App />
    , $('#app')[0])