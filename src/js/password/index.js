import React, {Component} from 'react'
import Root from '../util/root'
import ReactDom from 'react-dom'

class App extends Root {

    send() {
        let ws = plus.webview.currentWebview()
        plus.webview.close(ws)
    }

    render() {

        return (
            <div className="password">
                <img className="header" src="../src/img/octocat.png" />
                <div className="row">
                    <div className="input-field col s12">
                        <input id="email" type="email" className="validate" />
                            <label htmlFor="email">Email</label>
                    </div>
                    <span className="remind">输入邮箱以找回密码。</span>
                </div>
                <a className="waves-effect waves-light btn" onClick={this.send}>发送邮件</a>
            </div>
        )
    }
}

class M {
}

ReactDom.render(<App />, $('#app')[0])