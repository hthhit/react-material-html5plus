import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
    }

    componentDidMount() {
        document.addEventListener('plusready', () => {
            // console.log(plus.webview.currentWebview().index)
        })
    }

    render() {
        return (
            <div className="articleview">
                {this.getHeader('作品预览')}
                <Content />
            </div>
        )
    }
}

class Content extends Component {

    render() {
        return (
            <div className="content-container grey lighten-5">
                <div className="card">
                    <img className="img" src="img/scenery.jpg" />
                    <div className="container-origin">
                        <div className="container-title">
                            <div className="title">Scenery Of Our Nature</div>
                            <div className="time">5 minutes ago</div>
                        </div>
                        <img className="origin-header" src="img/octocat.png" />
                    </div>
                    <p>
                        这里填写说明文字。。。BalaBala...<br />
                        这里填写说明文字。。。BalaBala...<br />
                        这里填写说明文字。。。BalaBala...<br />
                        这里填写说明文字。。。BalaBala...
                    </p>
                    <div className="card-action">
                        <a href="#"><i className="material-icons">star</i></a>
                        <a href="#"><i className="material-icons">chat_bubble_outline</i></a>
                        <a href="#"><i className="material-icons">perm_identity</i></a>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, $('#app')[0])