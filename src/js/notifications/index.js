import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            notifications: null
        }
    }

    componentDidMount() {
        this.setState({
            notifications: ['item0', 'item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8', 'item9']
        })
    }

    render() {
        let _notifications = this.state.notifications

        return (
            <div className="notifications">
                {this.getHeader('通知消息')}
                <Content items={_notifications}/>
            </div>
        )
    }
}

class Content extends Component {

    getItems() {
        if (!this.props.items) return null
        let _items = this.props.items.map((item, index) => {
            return (
                <div className="card horizontal" key={index}>
                    <div className="card-image">
                        <img className="image" src="img/octocat.png"/>
                    </div>
                    <div className="card-stacked">
                        <div className="card-content">
                            <p>I am notification with {item}.</p>
                        </div>
                        <div className="card-action">
                            <a href="#">Link to {item}</a>
                        </div>
                    </div>
                </div>
            )
        })

        return _items
    }

    render() {
        let _items = this.getItems()

        return (
            <div className="content-container">
                {_items}
            </div>
        )
    }
}

ReactDom.render(
    <App />
    , $('#app')[0])