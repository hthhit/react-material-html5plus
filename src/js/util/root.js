/**
 * Created by Zwei on 2017/7/6.
 */
import React, {Component} from 'react'

export default class Root extends Component {
    constructor() {
        super()
        this.plus()
        this.header = Header //私有，仅 Root 拥有，其他不要修改
    }

    getHeader(title) {
        return <this.header title={title} />
    }

    //plus setting
    plus() {
        document.addEventListener('plusready', () => {
            plus.key.addEventListener("backbutton", function() {
                let ws=plus.webview.currentWebview()
                plus.webview.close(ws)
            })
        }, false)
    }
}

class Header extends Component {
    close() {
        let ws = plus.webview.currentWebview()
        plus.webview.close(ws)
    }

    render() {
        let _title = this.props.title

        return (
            <nav>
                <div className="nav-wrapper" onClick={this.close}>
                    <a href="#" className="brand-logo left"><i className="material-icons back">trending_flat</i></a>
                    <span className="title">{_title}</span>
                </div>
            </nav>
        )
    }
}