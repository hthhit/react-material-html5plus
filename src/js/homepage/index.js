import React, {Component} from 'react'
import ReactDom from 'react-dom'

class App extends Component {

    componentDidMount() {
        $('.button-collapse').sideNav({
            menuWidth: 300,
            edge: 'left',
            closeOnClick: true,
            draggable: true,
            onOpen: function(el) {

            },
            onClose: function(el) {

            }
        })
    }

    nextPage(index) {
        M.nextPage(index)
    }

    render() {
        return (
            <div className="homepage">
                <a href="#" data-activates="slide-out" className="button-collapse"><i className="material-icons menu">reorder</i></a>
                <div className="title">网格列表</div>
                <SideNav nextPage={this.nextPage} />
                <Content nextPage={this.nextPage} />
            </div>
        )
    }
}

class List extends Component {
    constructor(props) {
        super(props)
        this.nextPage = this.props.nextPage
    }
}

class SideNav extends List {
    render() {
        let lis = M.items().map((item, index) => {
            return (
                <li className="collection-item" key={index}>
                    <div onClick={() => this.nextPage(index)}>
                        {item}
                        <a href="#!" className="secondary-content"><i className="material-icons">{M.itemIcons()[index]}</i></a>
                    </div>
                </li>
            )
        })

        return (
            <ul id="slide-out" className="side-nav collection with-header">
                <li className="collection-header">
                    <h4>UI Koala</h4>
                </li>
                {lis}
            </ul>
        )
    }
}

class Content extends List {
    render() {
        let items = M.items().map((item, index) => {
            return (
                <div className="item" key={index} onClick={() => this.nextPage(index)}>
                    <i className="material-icons">{M.itemIcons()[index]}</i>
                    <span>{item}</span>
                </div>
            )
        })

        return (
            <div className="content">
                {items}
            </div>
        )
    }
}

class M {
    static items() {
        return ["账号相关", "社交", "作品", "消息", "控制台", "启动页", "经济", "导航", "其他"]
    }

    static itemIcons() {
        return ["lock", "assignment_ind", "receipt", "message", "dashboard", "work", "credit_card", "navigation", "dialpad"]
    }

    static nextPage(index) {
        let url = 'list.html'
        let obj = null

        switch (index) {
            case 0:
                obj = {
                    list: [
                        {
                            name: '登录',
                            url: 'loginv1.html'
                        },
                        {
                            name: '注册',
                            url: 'regist.html'
                        },
                        {
                            name: '找回密码',
                            url: 'password.html'
                        }
                    ]
                }
                break
            case 1:
                obj = {
                    list: [
                        {
                            name: '个人页',
                            url: 'userprofilev1.html'
                        },
                        {
                            name: '个人设置',
                            url: 'profilesetting.html'
                        },
                        {
                            name: '消息通知',
                            url: 'notifications.html'
                        },
                        {
                            name: '联系人',
                            url: 'contacts.html'
                        },
                        {
                            name: '卡片内容',
                            url: 'cards.html'
                        }
                    ]
                }
                break
            case 2:
                obj = {
                    list: [
                        {
                            name: '作品列表',
                            url: 'articlelist.html'
                        },
                        {
                            name: '文章列表',
                            url: 'blogposts.html'
                        },
                        {
                            name: '作品预览',
                            url: 'articleview.html'
                        }
                    ]
                }
                break
            case 5:
                url = 'list.html'
                break
            default:
                break
        }
        let ws = plus.webview.create(url, url, null, obj) //id 也使用 url
        plus.webview.show(ws, "slide-in-right")
    }
}

ReactDom.render(<App />, $('#app')[0])