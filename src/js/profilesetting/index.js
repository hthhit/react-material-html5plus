import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    render() {
        return (
            <div className="profilesetting">
                {this.getHeader('个人设置')}
                <Content />
            </div>
        )
    }
}

class Content extends Component {

    render() {
        return (
            <div className="content-container">
                <Info />
                <Password />
                <Connect />
                <a className="save" onClick={this.regist}>
                    SAVE
                </a>
            </div>
        )
    }
}

class Info extends Component {
    render() {
        return (
            <div className="container">
                <img className="responsive-img circle" src="img/octocat.png" />
                <div className="info-container">
                    <span className="info">INFO</span>
                </div>
                <div className="input-field">
                    <input id="last_name" type="text" className="validate" defaultValue="zwei" />
                    <label className="active" htmlFor="last_name">First Name</label>
                </div>
                <div className="input-field">
                    <input id="last_name" type="text" className="validate" defaultValue="zhao" />
                    <label className="active" htmlFor="last_name">Last Name</label>
                </div>
                <div className="input-field">
                    <input id="last_name" type="email" className="validate" defaultValue="zweizhao@foxmail.com" />
                    <label className="active" htmlFor="last_name">Email</label>
                </div>
                <div className="input-field">
                    <input id="last_name" type="text" className="validate" defaultValue="CN" />
                    <label className="active" htmlFor="last_name">Country</label>
                </div>
                <div className="input-field">
                    <input id="last_name" type="number" className="validate" defaultValue="0123456789" />
                    <label className="active" htmlFor="last_name">Phone</label>
                </div>
            </div>
        )
    }
}


class Password extends Component {
    render() {
        return (
            <div className="container">
                <div className="info-container">
                    <span className="info">PASSWORD</span>
                </div>
                <div className="input-field">
                    <input id="last_name" type="password" className="validate" defaultValue="123456" />
                    <label className="active" htmlFor="last_name">Old Password</label>
                </div>
                <div className="input-field">
                    <input id="last_name" type="password" className="validate" defaultValue="0123456789" />
                    <label className="active" htmlFor="last_name">NewPassword</label>
                </div>
                <div className="input-field">
                    <input id="last_name" type="password" className="validate" defaultValue="0123456789" />
                    <label className="active" htmlFor="last_name">ConfirlPassword</label>
                </div>
            </div>
        )
    }
}

class Connect extends Component {
    render() {
        return (
            <div className="container">
                <div className="info-container">
                    <span className="info">CONNECT</span>
                </div>
                <div className="switch">
                    <img className="logo" src="img/twitter.png" />
                    <label>
                        <input type="checkbox" />
                        <span className="lever"></span>
                    </label>
                </div>
                <div className="switch">
                    <img className="logo" src="img/google.png" />
                    <label>
                        <input type="checkbox" />
                        <span className="lever"></span>
                    </label>
                </div>
                <div className="switch">
                    <img className="logo" src="img/facebook.png" />
                    <label>
                        <input type="checkbox" />
                        <span className="lever"></span>
                    </label>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, $('#app')[0])