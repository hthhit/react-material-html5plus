import React, {Component} from 'react'
import Root from '../util/root'
import ReactDom from 'react-dom'

class App extends Root {

    regist() {
        let ws = plus.webview.create('regist.html', 'regist.html', null, {})
        plus.webview.show(ws || 'regist.html', "slide-in-right")
    }

    render() {

        return (
            <div className="loginv1">
                <div className="login-types">
                    <img src="img/twitter.png" />
                    <img src="img/google.png" />
                    <img src="img/facebook.png" />
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="username" type="text" className="validate" />
                            <label htmlFor="username">Username</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="password" type="password" className="validate" />
                            <label htmlFor="password">Password</label>
                    </div>
                </div>
                <a className="waves-effect waves-light btn">login</a>
                <div className="sign">
                    还没有账号？
                    <a className="regist" onClick={this.regist}>
                        点击注册
                    </a>
                </div>
            </div>
        )
    }
}

class M {
}

ReactDom.render(<App />, $('#app')[0])