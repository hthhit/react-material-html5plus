import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Root from '../util/root'

class App extends Root {

    constructor() {
        super()
        this.state = {
            cards: null
        }
    }

    componentDidMount() {
        this.setState({
            cards: ['item0', 'item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8', 'item9']
        })
    }

    render() {
        let _cards = this.state.cards

        return (
            <div className="articlelist">
                {this.getHeader('作品列表')}
                <Content items={_cards}/>
            </div>
        )
    }
}

class Content extends Component {

    goArticleView(index) {
        let url = "articleview.html"
        let ws = plus.webview.create(url, url, null, {index: index}) //id 也使用 url
        plus.webview.show(ws, "slide-in-right")
    }

    getItems() {
        if (!this.props.items) return null

        let _goArticleView = this.goArticleView.bind(this)
        let _items = this.props.items.map((item, index) => {
            return (
                <div className="card" key={index}>
                    <div className="card-image">
                        <img src="img/scenery.jpg" />
                        <a className="btn-floating btn-large halfway-fab waves-effect waves-light red">
                            <i className="material-icons" onClick={() => _goArticleView(index)}>add</i>
                        </a>
                    </div>
                    <div className="card-content">
                        <span className="card-title">{item}</span>
                        <p>作品简介 --- {item}</p>
                    </div>
                </div>
            )
        })

        return _items
    }

    render() {
        let _items = this.getItems()

        return (
            <div className="content-container grey lighten-5">
                {_items}
            </div>
        )
    }
}

ReactDom.render(
    <App />
    , $('#app')[0])