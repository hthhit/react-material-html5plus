# 一个基于 React，Materialize，HTML5Plus 的 UI 页面展示框架

![preview](src/img/readme/preview.gif)

## 使用方式：

* 网页端

[Homepage-请修改路径](https://zweizhao.github.io/react-material-html5plus/src/homepage.html)

> 直接打开文件的 html 格式文件预览即可
> 
> 因为需要 HTML5Plus 的 plus 变量，Web 端不存在，所以无法动态切换

* 手机端

[Android-直接下载](http://zweizhao.com/app/rmh5p.apk)

> 安装 HBuilder 打开项目
> 
> 安装到模拟器或手机上即可
